function pi_final = method2(sig_fig)

pi_eval = [];    % record all the pi values
points_in = [];  % record all the points inside the circle
points_out = []; % record all the points outside the circle
count = 0;  % the number of points inside the circle
total = 0;  % the number of total points
precision_thr = 10^(-sig_fig); % precision that user requests
precision = 1000;   % initialize precision
while precision > precision_thr
    total = total+1;
    
    % generate random point
    px = rand(1);
    py = rand(1);
        
    if (px^2 + py^2 <= 1)
        count = count+1;
        points_in = [points_in; [px, py]];
    else
        points_out = [points_out; [px, py]];
    end
    pi_eval = [pi_eval,count/total*4];
    if(mod(total,100)==0) % calculate the precision every 100 PIs
        precision = std(pi_eval(total-99:total)); % use standard deviation 
    end
end
pi_final = pi_eval(total);
fprintf('To get the %d significant figures, we need %d random points. The calculated PI is %f.\n', sig_fig, total, pi_final);

%% Plot the randon points
figure, 
for i = 1:size(points_in,1)
    plot(points_in(i,2), points_in(i,1), 'r.')
    hold on;
end

for i = 1:size(points_out,1)
    plot(points_out(i,2), points_out(i,1), 'b.')
    hold on;
end

th = 0:pi/50000:pi/2;
xunit = cos(th);
yunit = sin(th);
plot(xunit, yunit, 'k.');
title(sprintf('To get the %d significant figures, we need %d random points. The calculated PI is %f.\n', sig_fig, total, pi_calculated))



