1, To run ��method1.m��, 

>>pi_final = method1(10000)��

which means applying 10000 random points to calculate PI. 10000 can be replaced by any number. The final result of PI value is ��pi_final��. The code also plots the calculated PI, the errors and running time along with the different number of random points. 



2, To run ��method2.m��, 

>>pi_final = method2(3)

where 3 is the significant figures and it is given by user. The code with stop once the precision level reached the user��s request. The code also plots all the random points used for calculating PI, and prints the number of random points needed and the final calculated PI value as well.

