Project 2

Two operations forward elimination and back-substitution are implemented as ��fwd_elim.m�� and ��bk_subs.m��


��test_1_a.m�� is the function that checks whether a given matrix is in an echelon form, a row canonical form, or neither. 
Use it as: 
>> test_1_a(A)
where A is the matrix you want to check.


��test_1_b.m�� is the function that returns a row echelon form for any given input matrix. It just uses function ��fwd_elim��.
Use it as: 
>>test_1_b(A)


��test_1_c.m�� is the function that returns return the row canonical form for any given row echelon matrix. 
Use it as: 
>>test_1_c(A)


��test_1_d.m�� is the function that returns the solution of a linear equations. 
Use it as: 
>> solution = test_1_d(A, b)
where A is the matrix you want to check.


��test_1_e.m�� is used to test the forward elimination and back-substitution operations with random matrices of various sizes, and plot the time required to solve the system as a function of the number of rows, m, and columns, n. Two augments can be picked by user, the maximal matrix size and repeat number. As the running time is not stable, I test multiple times and show the average running time as the results. ��repeat number�� is how many times you want to test for each matrix size setting.
I also show the example plots with maximal number of 50 (the largest matrix size is 50*50) and repeat time is 100.
>> test_1_e(max_size, repeat)


The function ��myINV�� returns the inverse of a given input matrix. 
>> inv = myINV(A,w)
If A is txt file name, function will read matrix from the txt file. If ��w�� is a txt file, the function will save the result to the txt file.


>> [L,U,P] = myLU(A)

