function [res, elem] = bk_subs(eche, pivot)

elem = eye(size(eche,1));
% find the starting row
for i = 1:size(eche,1)         % searching i
   if last_non_zero_row(eche,i)
       break;
   end
end


while i > 1
    for j = 1:size(eche,2)  % searching j
        if pivot(i,j) == 1
            break;
        end
    end
    eche(i,:) = eche(i,:) / eche(i,j);
    elem(i,:) = elem(i,:) / eche(i,j);
    for k = i-1:-1:1
        eche(k,:) = eche(k,:)-eche(k,j)*eche(i,:);
        elem(k,:) = elem(k,:)-eche(k,j)*elem(i,:);
    end
    % update i
    i = i-1;
end
res = eche;


