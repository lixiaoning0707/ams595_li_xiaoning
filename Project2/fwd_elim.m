function [res, pivot, P, M, elem] = fwd_elim(A)

i = 1;
j = 1;
idt = eye(size(A,1)); % identity matrix with the same size of A
P = idt;  % P matrix, initialized as identity matrix
M = idt;  % M matrix, initialized as identity matrix
elem = idt; % elementary matrix, initialized as identity matrix
pivot = zeros(size(A));
while i <= size(A,1) && j <= size(A,2)
       if A(i,j) == 0  % step 2
           % go to step 3
           [v,idx] = max(A(i+1:size(A,1),j));
           if v ~= 0 % found a non-zero
               A([i, idx+i],:) = A([idx+i, i],:);  % swap the row i and i+idx
               elem_cur = idt;
               elem_cur([i, idx+i],:) = elem_cur([idx+i, i],:);
               P = P * elem_cur;
               elem = elem * elem_cur;
               
               % go to step 5
               pivot(i,j) = 1;
               for k = i+1:size(A,1)
                  factor = A(k,j)/A(i,j);
                  A(k,:) = A(k,:) - factor .* A(i,:); % zero out
                  elem(k,:) = elem(k,:) - factor .* elem(i,:);
                  M(k,:) = M(k,:) - factor .* M(i,:);
               end
           else      % not found
               j = j+1;
               continue;
           end
       else % go to step 5
           pivot(i,j) = 1;
           for k = i+1:size(A,1)
               factor = A(k,j)/A(i,j); 
               A(k,:) = A(k,:) - factor .* A(i,:); % zero out
               elem(k,:) = elem(k,:) - factor .* elem(i,:);
               M(k,:) = M(k,:) - factor .* M(i,:);
           end
       end
       if ~last_non_zero_row(A, i)
           i = i+1;
           j = j+1; 
       else
           res = A;
           return;
       end
end
res = A;
