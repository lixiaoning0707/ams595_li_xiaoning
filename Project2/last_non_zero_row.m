function res = last_non_zero_row(A, i)

if i == size(A, 1)
   res = 1; 
else
    matrix_below = A(i+1:size(A,1),:);
    if any(matrix_below(:))
        res = 0;
    else
        res = 1;
    end
end