function inv = myINV(A,w)

if ischar(A)
    A = importdata(A);
end
if size(A,1) ~= size(A,2)
    fprintf('Matrix is not square.\n')
    return;
end
[res, pivot, P, M, elem_fwd] = fwd_elim([A,eye(size(A,1))]);
[res, elem_bk] = bk_subs(res, pivot);
inv = res(:,size(A,1)+1:end);

if ~strcmp(w,'None')
    dlmwrite(w,inv);
end