function [L,U,P] = myLU(A)

[res, pivot, P, M, elem] = fwd_elim(A);
U = M*A;
L = myINV(M);