function test_1_a(A)

if isRow_Canonical(A)
   fprintf('It is both echelon form and row-canonical form.\n');
elseif isequal(A,fwd_elim(A))
   fprintf('It is echelon form.\n');
else
   fprintf('It is neither echelon form or row-canonical form.\.');
end


function res = isRow_Canonical(M)

for i = 1:size(M,1)
    j = 1;
    while M(i,j) == 0
        j = j+1;
    end
    if M(i,j) ~= 1
        res = 0;
        return;
    else
        if any([M(1:i-1,j) M(i+1:end,j)])
            res = 0;
            return;
        end
    end
end
res = 1;

