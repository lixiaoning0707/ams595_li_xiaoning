function res = test_1_c(A)

[eche, pivot, P, M, elem_fwd] = fwd_elim(A);
if ~isequal(eche,A)
   fprintf('It is not a echelon form.\n') 
end

[res, elem_bk] = bk_subs(eche, pivot);
