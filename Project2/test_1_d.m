function solution = test_1_d(A, b)

if size(A,2) > size(A,1)
    fprintf('Variable number greater than the number of equations.\n')
    return;
end

[res, pivot, P, M, elem_fwd] = fwd_elim([A,b]); % combining the A and b first
[res, elem_bk] = bk_subs(res, pivot);
solution = res(:,size(A,1)+1:end);