function [run_times_fwd, run_times_bk] = test_1_e(max_size,rep_times)

run_times_fwd = zeros([max_size, max_size]);
run_times_bk = zeros([max_size, max_size]);
for i = 2:max_size
    for j = 2:max_size
        temp_fwd = 0;
        temp_bk = 0;
        for k = 1:rep_times
            start_time = tic;
            [res, pivot] = fwd_elim(randi(10,[i,j]));
            run_time = toc(start_time);
            temp_fwd = temp_fwd + run_time;
            start_time = tic;
            bk_subs(res, pivot);
            run_time = toc(start_time);
            temp_bk = temp_bk + run_time;
        end
        run_times_fwd(i,j) = temp_fwd / rep_times;
        run_times_bk(i,j) = temp_bk / rep_times; 
    end
end

figure,
[X, Y] = meshgrid(1:max_size, 1:max_size);
mesh(X, Y, run_times_fwd)
xlabel('Row dimension','Rotation',15),axis([0 max_size 0 max_size 0 max(run_times_fwd(:))*1.2])
ylabel('Column dimension','Rotation',-25)
zlabel('Time(s)');

figure,
[X, Y] = meshgrid(1:max_size, 1:max_size);
mesh(X, Y, run_times_bk)
xlabel('Row dimension','Rotation',15),axis([0 max_size 0 max_size 0 max(run_times_bk(:))*1.2])
ylabel('Column dimension','Rotation',-25)
zlabel('Time(s)');

